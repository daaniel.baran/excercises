package baran.daniel.interviews;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CountMinutesBetweenTime {

    public static void main(String[] args) {

        try {
            System.out.println(countMinutes("12:20AM-02:40PM"));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }

    private static String countMinutes(String input) throws ParseException {
        String[] times = input.split("-");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mma");

        LocalTime time1 = LocalTime.parse(times[0], formatter);
        LocalTime time2 = LocalTime.parse(times[1], formatter);

        long differenceInMinutes = Duration.between(time1, time2).toMinutes();

        return String.valueOf(differenceInMinutes);
    }

    @Test
    void shouldGiveCorrectAmountOfMinutes() throws ParseException {
        String time1 = "12:20AM-02:40PM";
        String time2 = "11:20AM-02:40PM";

        assertTrue(countMinutes(time1).equals("860"));
        assertTrue(countMinutes(time2).equals("200"));
    }

}
