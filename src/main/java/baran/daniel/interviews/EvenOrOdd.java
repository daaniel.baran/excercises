package baran.daniel.interviews;

public class EvenOrOdd {

    static int maxSwitchingLength(int[] intArray) {
        int max = 1;
        int tempMax = 1;

        for (int i = 0; i < intArray.length - 1; i++) {
            int current = intArray[i];
            int next = intArray[i + 1];

            if ((current + next) % 2 == 0)
                tempMax = 1;
            else tempMax++;

            max = Math.max(max, tempMax);
        }
        return max;
    }

    public static void main(String[] args) {
        int[] test1 = {3, 4, 3, 5, 4, 3, 4, 3, 2, 4};
        int[] test2 = {3, 4, 2, 4, 3, 4, 3, 5, 5, 5, 1, 2, 1, 2, 1, 2};

        System.out.println(maxSwitchingLength(test1));
        System.out.println(maxSwitchingLength(test2));
    }

}
