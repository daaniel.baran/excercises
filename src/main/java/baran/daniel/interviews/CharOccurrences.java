package baran.daniel.interviews;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CharOccurrences {


    public static void main(String[] args) {
        System.out.println(printOccurrences("4c 2a 1b 4a 2c 2b 2d 1e 2s 1a"));
    }

    public static String printOccurrences(String str) {

        char[] chars = str.toCharArray();
        StringBuilder output = new StringBuilder();
        int counter = 1;

        for (int index = 0; index < chars.length - 1; index++) {
            char current = chars[index];
            char next = chars[index + 1];

            if (index == chars.length - 2) {
                if (next != current) {
                    output.append(counter).append(current).append(1).append(next);
                } else output.append(++counter).append(next);
            } else {
                if (next != current) {
                    output.append(counter).append(current);
                    counter = 1;
                } else
                    counter++;
            }

        }
        return output.toString();
    }

    @Test
    void shouldReplaceInputWithCorrectPattern() {
        String input = "aabbcccdeffssaa";
        String input1 = "abbcccdeffssab";

        String expectedOutput = "2a2b3c1d1e2f2s2a";
        String expectedOutput1 = "1a2b3c1d1e2f2s1a1b";

        assertTrue(printOccurrences(input).equals(expectedOutput));
        assertTrue(printOccurrences(input1).equals(expectedOutput1));
    }
}
