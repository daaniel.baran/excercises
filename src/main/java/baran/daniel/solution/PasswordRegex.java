package baran.daniel.solution;
import java.util.Scanner;

public class PasswordRegex {
    class UsernameValidator {
        /*
         * Write regular expression here.
         */
        public static final String regularExpression = "[^0-9_][0-9a-zA-Z_]{7,29}";
    }



}

class Sol {
    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scan.nextLine());
        while (n-- != 0) {
            String userName = scan.nextLine();

            if (userName.matches(PasswordRegex.UsernameValidator.regularExpression)) {
                System.out.println("Valid");
            } else {
                System.out.println("Invalid");
            }
        }
    }
}
