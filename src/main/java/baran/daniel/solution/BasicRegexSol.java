package baran.daniel.solution;

import java.util.Scanner;

public class BasicRegexSol {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String IP = in.next();
            System.out.println(IP.matches(new MyRegex().pattern));
        }

    }
}

class MyRegex {

    String pattern = "[0-9]{1,3}|255\\.[0-9]{1,3}|255\\.[0-9]{1,3}|255\\.[0-9]{1,3}|255";

}
