package baran.daniel.solution;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Scanner;

public class ArrayGame {
    public static final Path test = Path.of("/Users/daniel/IdeaProjects/hackerrank-playground/test");
    public static final Path result = Path.of("/Users/daniel/IdeaProjects/hackerrank-playground/result");


    public static boolean canWin(int leap, int[] game) {
        boolean result = false;
        int lastIndex = game.length - 1;
        int index = 0;

        if (leap > lastIndex) return true;

        while (game[index] == 0) {
            if (index + 1 <= lastIndex && game[index + 1] == 0) {
                game[index] = 1;
                index++;
            } else if (index + leap > lastIndex || index == lastIndex) return true;
            else if (leap > 0 && game[index + leap] == 0) {
                game[index] = 1;
                index += leap;
            } else if (index - 1 > 0 && game[index - 1] == 0) {
                game[index] = 1;
                index--;
            } else return false;
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        FileWriter fileWriter = new FileWriter("test");
        PrintWriter printWriter = new PrintWriter(fileWriter);

        Scanner scan = new Scanner(System.in);
        int q = scan.nextInt();
        while (q-- > 0) {
            int n = scan.nextInt();
            int leap = scan.nextInt();

            int[] game = new int[n];
            for (int i = 0; i < n; i++) {
                game[i] = scan.nextInt();
            }

//            System.out.println(((canWin(leap, game)) ? "YES " : "NO "));
            printWriter.println(((canWin(leap, game)) ? "YES " : "NO "));

        }
        printWriter.close();
        scan.close();

        try {
            System.out.println(CompareFiles.filesCompareByLine(test, result));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
